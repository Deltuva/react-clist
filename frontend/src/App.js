import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import NumberFormat from 'react-number-format';
import Pagination from "react-js-pagination";
import {
  MAX_PER_PAGE,
  MAX_PER_RANGE
} from './redux/actions/types';

import CompanyItem from './components/CompanyItem';
import Loading from './components/CompanyItem/Loading';
import { fetchCompaniesCount, fetchCompanies } from './redux/actions/companies';
import { Container, Row } from 'react-bootstrap';
import SearchForm from './components/Search/SearchForm';

const App = () => {
  const dispatch = useDispatch();

  const listOfCompanies = useSelector(({ companies }) => companies.items);
  const totalCompaniesInSearch = useSelector(({ companies }) => companies.itemsCnt);
  const totalCompanies = useSelector(({ companiesCount }) => companiesCount.total.cnt);
  const isSearched = useSelector(({ companies }) => companies.isSearched);
  const isLoaded = useSelector(({ companies }) => companies.isLoaded);

  const [searchQuery, setSearchQuery] = useState('');
  const [currentPage, setCurrentPage] = useState(1);
  const recordPerPage = MAX_PER_PAGE;
  const totalRecords = isSearched ? totalCompaniesInSearch || 0 : totalCompanies || 0;
  const pageRange = MAX_PER_RANGE;

  useEffect(() => {
    dispatch(fetchCompaniesCount());
  }, [dispatch]);

  useEffect(() => {
    dispatch(fetchCompanies(searchQuery, currentPage, recordPerPage));
  }, [dispatch, searchQuery, currentPage, recordPerPage]);

  const handlePageChange = pageNumber => {
    setCurrentPage(pageNumber);
  }

  const submitSearchHandle = e => {
    e.preventDefault();

    setSearchQuery(e.target.query.value);
  }

  const DataRender = () => {
    return (
      isLoaded ? <Loading /> :
        listOfCompanies && listOfCompanies.length !== 0 ? (
          listOfCompanies.map((obj) => (
            <CompanyItem
              key={ obj._id }
              { ...obj }
            />
          ))
        ) : (
            <div className="error">
              { `- Nerasta jokių įmonių. -` }
            </div>
          )
    );
  }

  return (
    <main>
      <Container fluid>
        <div className="main-results">
          <div className="main-results-title">
            <h2>{ `Įmonių sąrašas` }</h2>
            <p>{ `Iš viso duomenų bazėje` } <span>
              <NumberFormat
                value={ totalCompanies || 0 }
                displayType={ 'text' }
                thousandSeparator={ true }
              /></span></p>
          </div>
          <SearchForm
            searchQuery={ searchQuery }
            submitSearchHandle={
              e => submitSearchHandle(e)
            }
          />
          { isSearched && <p>{ `Rasta rezultatų:` } <span className="cnt">
            <NumberFormat
              value={ totalCompaniesInSearch || 0 }
              displayType={ 'text' }
              thousandSeparator={ true }
            /></span></p> }
          <div className="main-results-table">
            <div className="main-results-table-head">
              <Row className={ 'm-0' }>
                <div className="cell head min-width-20">
                  <span>{ `Įmonė` }</span>
                </div>
                <div className="cell head min-width-15">
                  <span>{ `Įmonės kodas` }</span>
                </div>
                <div className="cell head min-width-20">
                  <span>{ `Veiklos sritis` }</span>
                </div>
                <div className="cell head long">
                  <span>{ `Adresas` }</span>
                </div>
                <div className="cell head icon-cell">
                  <span>{ `N` }</span>
                </div>
              </Row>
            </div>
            <div className="main-results-table-body">
              <DataRender />
              { !isLoaded && totalRecords !== 0 && <Pagination
                hideDisabled
                itemClass="page-item"
                linkClass="page-link"
                activePage={ currentPage }
                itemsCountPerPage={ recordPerPage }
                totalItemsCount={ totalRecords }
                pageRangeDisplayed={ pageRange }
                onChange={ handlePageChange }
              />
              }
            </div>
          </div>
        </div>
      </Container>
    </main>
  );
};

export default App;
