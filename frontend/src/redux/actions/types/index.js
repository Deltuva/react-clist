const API_BACKEND = 'https://fast-temple-07597.herokuapp.com/api';

export const MAX_PER_PAGE = 25;
export const MAX_PER_RANGE = 5;

export const COMPANIES_COUNT_LIST_URL_POINT = `${API_BACKEND}/companies-count/1`;
export const COMPANIES_COUNT_LIST_REQUEST = 'COMPANIES_COUNT_LIST_REQUEST';
export const COMPANIES_COUNT_LIST_SUCCESS = 'COMPANIES_COUNT_LIST_SUCCESS';
export const COMPANIES_COUNT_LIST_FAILURE = 'COMPANIES_COUNT_LIST_FAILURE';

export const COMPANIES_LIST_URL_POINT = `${API_BACKEND}/companies`;
export const COMPANIES_LIST_REQUEST = 'COMPANIES_LIST_REQUEST';
export const COMPANIES_LIST_SUCCESS = 'COMPANIES_LIST_SUCCESS';
export const COMPANIES_LIST_FAILURE = 'COMPANIES_LIST_FAILURE';

export const COMPANY_DETAILS_URL_POINT = `${API_BACKEND}/companies/view`;
export const COMPANY_DETAILS_REQUEST = 'COMPANY_DETAILS_REQUEST';
export const COMPANY_DETAILS_SUCCESS = 'COMPANY_DETAILS_SUCCESS';
export const COMPANY_DETAILS_FAILURE = 'COMPANY_DETAILS_FAILURE';