import axios from 'axios';
import {
  COMPANIES_COUNT_LIST_URL_POINT,
  COMPANIES_COUNT_LIST_REQUEST,
  COMPANIES_COUNT_LIST_SUCCESS,
  COMPANIES_COUNT_LIST_FAILURE,
  COMPANIES_LIST_URL_POINT,
  COMPANIES_LIST_REQUEST,
  COMPANIES_LIST_SUCCESS,
  COMPANIES_LIST_FAILURE,
  COMPANY_DETAILS_URL_POINT,
  COMPANY_DETAILS_REQUEST,
  COMPANY_DETAILS_SUCCESS,
  COMPANY_DETAILS_FAILURE
} from './types';

export const fetchCompaniesCount = () => async (dispatch) => {
  try {
    dispatch({
      type: COMPANIES_COUNT_LIST_REQUEST
    });
    await axios.get(`${COMPANIES_COUNT_LIST_URL_POINT}`)
      .then(({ data }) => {
        dispatch({
          type: COMPANIES_COUNT_LIST_SUCCESS,
          payload: data
        });
      }).catch((error) => {
        dispatch({
          type: COMPANIES_COUNT_LIST_FAILURE,
          payload: error.message
        });
      });
  } catch (error) {
    console.log("throwing Error", error);
    throw error;
  }
};

export const fetchCompanies = (searchQuery, currentPage, perPage) => async (dispatch) => {
  const queryParams = {};

  queryParams["page"] = currentPage;
  queryParams["perPage"] = perPage;
  queryParams["searchQuery"] = searchQuery;

  try {
    dispatch({
      type: COMPANIES_LIST_REQUEST
    });
    await axios.post(`${COMPANIES_LIST_URL_POINT}`, queryParams)
      .then(({ data }) => {
        dispatch({
          type: COMPANIES_LIST_SUCCESS,
          payload: data.results,
          counter: data.resultsCnt,
          isSearched: data.isSearched
        });
      }).catch((error) => {
        dispatch({
          type: COMPANIES_LIST_FAILURE,
          payload: error.message
        });
      });
  } catch (error) {
    console.log("throwing Error", error);
    throw error;
  }
};

export const fetchCompanyWithId = (companyId) => async (dispatch) => {
  try {
    dispatch({
      type: COMPANY_DETAILS_REQUEST
    });
    await axios.get(`${COMPANY_DETAILS_URL_POINT}/${companyId}`)
      .then(({ data }) => {
        dispatch({
          type: COMPANY_DETAILS_SUCCESS,
          payload: data
        });
      }).catch((error) => {
        dispatch({
          type: COMPANY_DETAILS_FAILURE,
          payload: error.message
        });
      });
  } catch (error) {
    console.log("throwing Error", error);
    throw error;
  }
};
