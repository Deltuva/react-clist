import { combineReducers } from 'redux';

import companies from './companies';
import companiesDetail from './companiesDetail';
import companiesCount from './companiesCount';

const rootReducer = combineReducers({
  companies,
  companiesDetail,
  companiesCount
});

export default rootReducer;
