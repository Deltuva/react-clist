import {
  COMPANY_DETAILS_REQUEST,
  COMPANY_DETAILS_SUCCESS,
  COMPANY_DETAILS_FAILURE
} from '../actions/types';

const companiesDetail = (state = { detail: {} }, action) => {
  switch (action.type) {
    case COMPANY_DETAILS_REQUEST:
      return {
        isLoaded: true
      };

    case COMPANY_DETAILS_SUCCESS:
      return {
        isLoaded: false,
        detail: action.payload,
      };

    case COMPANY_DETAILS_FAILURE:
      return {
        isLoaded: false,
        error: action.payload,
      };

    default:
      return state;
  }
};

export default companiesDetail;
