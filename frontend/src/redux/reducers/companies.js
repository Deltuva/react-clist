import {
  COMPANIES_LIST_REQUEST,
  COMPANIES_LIST_SUCCESS,
  COMPANIES_LIST_FAILURE
} from '../actions/types';

const initialState = {
  items: [],
  itemsCnt: 0,
  isSearched: false,
  isLoaded: false
};

const companies = (state = initialState, action) => {
  switch (action.type) {
    case COMPANIES_LIST_REQUEST:
      return {
        ...state,
        isLoaded: true
      };

    case COMPANIES_LIST_SUCCESS:
      return {
        ...state,
        isLoaded: false,
        items: action.payload,
        itemsCnt: action.counter,
        isSearched: action.isSearched
      };

    case COMPANIES_LIST_FAILURE:
      return {
        ...state,
        isLoaded: false,
        error: action.payload
      };

    default:
      return state;
  }
};

export default companies;
