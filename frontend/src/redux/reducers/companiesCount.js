import {
  COMPANIES_COUNT_LIST_REQUEST,
  COMPANIES_COUNT_LIST_SUCCESS,
  COMPANIES_COUNT_LIST_FAILURE
} from '../actions/types';

const initialState = {
  total: 0
};

const companies = (state = initialState, action) => {
  switch (action.type) {
    case COMPANIES_COUNT_LIST_REQUEST:
      return {
        ...state
      };

    case COMPANIES_COUNT_LIST_SUCCESS:
      return {
        ...state,
        total: action.payload
      };

    case COMPANIES_COUNT_LIST_FAILURE:
      return {
        ...state,
        error: action.payload
      };

    default:
      return state;
  }
};

export default companies;
