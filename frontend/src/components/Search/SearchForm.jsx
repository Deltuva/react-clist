import React from 'react';
import { Form, InputGroup, FormControl, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'

const SearchForm = ({ submitSearchHandle }) => {
  return (
    <div className="search-form">
      <Form onSubmit={ submitSearchHandle }>
        <InputGroup className="search-form__group">
          <FormControl
            className="search-form__input"
            name="query"
            placeholder={ `Įmonės pavadinimas / kodas / vadovas` }

          />
          <InputGroup.Append>
            <Button className="search-form__btn" type="submit">
              <FontAwesomeIcon
                icon={ faSearch }
              /> { `Ieškoti` }
            </Button>
          </InputGroup.Append>
        </InputGroup>
      </Form>
    </div>
  );
};

export default SearchForm;