import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Container, Row } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';

const CompanyDetail = ({ name, namePrefix, status, ceo, code, scope, address, phoneNumber, email, website, registeredAt }) => {

  const statusName = () => {
    return !status ? 'wasted' : 'unwasted';
  }

  return (
    <Container fluid>
      <Row>
        <div className="info-table">
          <div className="info-table-name">
            <span>{ `${namePrefix} "${name}"` }</span>
          </div>
          <div className="info-table-head">
            <span>{ `Informacija apie įmonę` }</span>
          </div>
          <div className="info-table-row">
            <div className="info-table-column title">
              <h4>{ `Įmonės kodas:` }</h4>
            </div>
            <div className="info-table-column">
              <p>{ code }</p>
            </div>
          </div>
          <div className="info-table-row">
            <div className="info-table-column title">
              <h4>{ `Įmonės statusas:` }</h4>
            </div>
            <div className="info-table-column">
              <p>{ statusName() }</p>
            </div>
          </div>
          <div className="info-table-row">
            <div className="info-table-column title">
              <h4>{ `Įmonės registracijos data:` }</h4>
            </div>
            <div className="info-table-column">
              <p>{ registeredAt }</p>
            </div>
          </div>
          <div className="info-table-row">
            <div className="info-table-column title">
              <h4>{ `Įmonės vadovas:` }</h4>
            </div>
            <div className="info-table-column">
              <p>{ ceo }</p>
            </div>
          </div>
          <div className="info-table-row">
            <div className="info-table-column title">
              <h4>{ `Įmonės registracijos adresas:` }</h4>
            </div>
            <div className="info-table-column">
              <p>{ address }</p>
            </div>
          </div>
          <div className="info-table-row">
            <div className="info-table-column title">
              <h4>{ `Tel.nr.:` }</h4>
            </div>
            <div className="info-table-column">
              <p>{ phoneNumber  }</p>
            </div>
          </div>
          <div className="info-table-row">
            <div className="info-table-column title">
              <h4>{ `El. p.:` }</h4>
            </div>
            <div className="info-table-column">
              <p>{ email }</p>
            </div>
          </div>
          <div className="info-table-row">
            <div className="info-table-column title">
              <h4>{ `Tinklapis:` }</h4>
            </div>
            <div className="info-table-column">
              <p>{ website }</p>
            </div>
          </div>
          <div className="info-table-row">
            <div className="info-table-column title">
              <h4>{ `Veiklos sritis:` }</h4>
            </div>
            <div className="info-table-column">
              <p>{ scope }</p>
            </div>
          </div>
          <div className="info-table-footer">
            <Link to={ `/` }>
              <FontAwesomeIcon
                icon={ faArrowLeft }
                size="1x"
              /> { `Grįžti į pilną sąrašą` }
            </Link>
          </div>
        </div>
      </Row>
    </Container>
  );
};

CompanyDetail.propTypes = {
  name: PropTypes.string,
  code: PropTypes.string,
  scope: PropTypes.string,
  address: PropTypes.string,
  employees: PropTypes.number
};

export default CompanyDetail;