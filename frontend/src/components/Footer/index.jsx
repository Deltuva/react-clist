import React from 'react';

const Footer = () => {
  const copy = `http://deltuva.com`;

  return (
    <footer className="footer">
      <p>Created by: <a href={ copy }>Deltuva</a></p>
    </footer>
  );
};

export default Footer;