import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import { fetchCompanyWithId } from '../../redux/actions/companies';
import CompanyDetail from '../CompanyDetail';
import Loading from '../CompanyDetail/Loading';

const CompanyView = () => {
  const dispatch = useDispatch();
  const companiesDetails = useSelector(state => state.companiesDetail);
  const { detail, isLoaded, error } = companiesDetails;
  const history = useHistory();
  const params = useParams();
  const id = params.id;

  useEffect(() => {
    if (id) {
      dispatch(fetchCompanyWithId(id));
    } else {
      history.push(`/`);
    }
  }, [history, dispatch, id]);

  return (
    <div>
      { isLoaded ? <Loading /> :
        error ? <div>{ error }</div> :
        <CompanyDetail { ...detail } /> }
    </div>
  );
};

export default CompanyView;