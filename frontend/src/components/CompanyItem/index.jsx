import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const CompanyItem = ({ _id, name, namePrefix, code, scope, address, employees }) => {

  const truncateScope = (str) => {
    const MAX_LENGTH = 25;
    return str.length > MAX_LENGTH ? str.substring(0, MAX_LENGTH) + "..." : str;
  }

  return (
    <Link className="row" to={ `/imone/${_id}` }>
      <div className="cell min-width-20">
        <span className="label">
          <strong>{ `Įmonė:` }</strong>
        </span>
        <span>{ `${namePrefix} "${name}"` }</span>
      </div>
      <div className="cell min-width-15">
        <span className="label">
          <strong>{ `Įmonės kodas:` }</strong>
        </span>
        <span>{ code }</span>
      </div>
      <div className="cell min-width-20">
        <span className="label">
          <strong>{ `Veiklos sritis:` }</strong>
        </span>
        <span>{ truncateScope(scope) }</span>
      </div>
      <div className="cell long">
        <span className="label">
          <strong>{ `Adresas:` }</strong>
        </span>
        <span>{ address }</span>
      </div>
      <div className="cell icon-cell">
        <span className="label">
          <strong>{ `Darbuotojų sk.:` }</strong>
        </span>
        <span>{ employees }</span>
      </div>
    </Link>
  );
};

CompanyItem.propTypes = {
  name: PropTypes.string,
  code: PropTypes.string,
  scope: PropTypes.string,
  address: PropTypes.string,
  employees: PropTypes.number
};

export default CompanyItem;