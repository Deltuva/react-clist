import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Provider } from 'react-redux';

import store from './redux/store';

import './scss/app.scss';
import App from './App';
import CompanyView from './components/CompanyView';
import Page404 from './pages/404';
import Footer from './components/Footer';

ReactDOM.render(
  <Router>
    <Provider store={ store }>
      <Switch>
        <Route path="/" exact component={ App } />
        <Route path="/imone/:id" component={ CompanyView } />
        <Route component={ Page404 } />
      </Switch>
      <Footer />
    </Provider>
  </Router>,
  document.getElementById('root'),
);
