import express from "express";
import cors from "cors";
import "./config/db.js";
import Company from "./modules/Company.js";
import fakeData from "./modules/data/FakeData.js";

const app = express();
const PORT = process.env.PORT || 9998;

app.use(express.json());
app.use(cors());

app.get(`/`, (req, res) => res.status(200).send("Server.js started.."));

app.get(`/api/companies-count/:count?`, async (req, res) => {
  if (req.params.count) {
    let condition = {};

    Company.find(condition)
      .countDocuments()
      .then(data => {
        res.status(200).send({
          "cnt": data
        });
      })
      .catch(err => {
        res.status(500).send({
          message:
            err || `Some error ${err}  occurred while retrieving data.`
        });
      });
  }
});

app.post(`/api/fake-companies`, async (req, res) => {
  Company.insertMany(fakeData)
    .then(data => {
      res.status(200).send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err || `Some error ${err}  occurred while retrieving data.`
      });
    });
});

app.post(`/api/companies`, async (req, res) => {

  const pagination = req.body.perPage ? parseInt(req.body.perPage) : 25;
  const pageNumber = req.body.page ? parseInt(req.body.page) : 1;
  const searchQuery = req.body.searchQuery ? req.body.searchQuery : '';

  let condition = {};

  if (searchQuery) {
    condition = {
      $or: [
        { 'name': { $regex: new RegExp(`^${searchQuery}$`, 'i') } },
        { 'code': searchQuery },
        { 'ceo': { $regex: new RegExp(`^${searchQuery}$`, 'i') } }
      ]
    };
  }

  const countSearch = await Company.countDocuments(condition);

  const query = Company.find({});
  if (searchQuery) {
    query.or([
      { 'name': { $regex: new RegExp(`^${searchQuery}$`, 'i') } },
      { 'code': searchQuery },
      { 'ceo': { $regex: new RegExp(`^${searchQuery}$`, 'i') } }
    ]);
  }
  query.sort({ _id: -1 })
  query.skip((pageNumber - 1) * pagination)
  query.limit(pagination)
    .then(data => {
      res.status(200).send({
        "results": data,
        "resultsCnt": countSearch,
        "isSearched": searchQuery ? true : false
      });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err || `Some error ${err}  occurred while retrieving data.`
      });
    });
});

app.get(`/api/companies/view/:id`, async (req, res) => {
  Company.findOne({ '_id': req.params.id })
    .then(data => {
      res.send(data);
    }).catch(err => {
      res.status(404).send({ msg: `Company Not Found.` });
    });
});

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
