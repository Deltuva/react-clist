import mongoose from "mongoose";

const companySchema = mongoose.Schema({
  name: String,
  namePrefix: String,
  status: Boolean,
  ceo: String,
  debt: Number,
  code: String,
  scope: String,
  address: String,
  employees: Number,
  phoneNumber: String,
  email: String,
  website: String,
  registeredAt: String,
  createdAt: Date
}, { versionKey: false });

export default mongoose.model('companies', companySchema);