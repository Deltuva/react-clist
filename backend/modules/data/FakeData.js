import faker from "faker";

const max = 50000;
const fakes = [];

const randomPrefix = () => {
  let arr = ['MB', 'UAB', 'AB', 'IV'];
  return arr[Math.floor(Math.random() * arr.length)];
}

const randomStatus = () => {
  let arr = [false, true];
  return arr[Math.floor(Math.random() * arr.length)];
}

for (let fake = 0; fake < max; fake++) {
  fakes[fake] = {
    name: faker.company.companyName(),
    namePrefix: randomPrefix(),
    status: randomStatus(),
    ceo: faker.name.findName(),
    debt: faker.random.number(9000000),
    code: faker.random.number(1000000),
    scope: faker.lorem.words(25),
    address: faker.address.streetAddress(),
    employees: faker.random.number(1000),
    phoneNumber: faker.phone.phoneNumber(),
    email: faker.internet.email(),
    website: faker.internet.url(),
    registeredAt: `${faker.date.recent()}`,
    createdAt: Date.now()
  };
}

export default fakes;